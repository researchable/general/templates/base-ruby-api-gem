def httparty_response(response_data)
  response = double('reponse')
  allow(response).to receive(:[]).and_return []
  allow(response).to receive(:body).and_return response_data
  allow(response).to receive(:code).and_return 200
  allow(response).to receive(:parsed_response).and_return response_data
  response
end
