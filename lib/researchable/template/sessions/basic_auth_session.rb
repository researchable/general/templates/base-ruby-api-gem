module Researchable
  module Template
    module Sessions
      class BasicAuthSession
        attr_reader :api_url
        attr_reader :username
        attr_reader :password

        def initialize(api_url: ENV['SVC_PREDICTOR_URL'],
                       username: ENV['SVC_PREDICTOR_BASICAUTH_ID'],
                       password: ENV['SVC_PREDICTOR_BASICAUTH_SECRET'])
          @api_url = api_url
          @username = username
          @password = password
        end

        def get(path, params = {})
          perform_request_or_fail do
            HTTParty.get(full_url_for(path),
                         query: params,
                         basic_auth: basic_auth)
          end
        end

        def post(path, params = {})
          perform_request_or_fail do
            HTTParty.post(full_url_for(path),
                          headers: {'Content-Type' => 'application/json'},
                          body: params.to_json,
                          basic_auth: basic_auth)
          end
        end

        def patch(path, params = {})
          perform_request_or_fail do
            HTTParty.patch(full_url_for(path),
                           headers: {'Content-Type' => 'application/json'},
                           body: params.to_json,
                           basic_auth: basic_auth)
          end
        end

        def delete(path, params = {})
          perform_request_or_fail do
            HTTParty.delete(full_url_for(path),
                            query: params,
                            basic_auth: basic_auth)
          end
        end

        private

        def perform_request_or_fail(&block)
          response = yield
          case response.code
          when 200..299, 422
            response
          when 401
            access_denied(response)
          else
            fail response.parsed_response || "Received HTTP response code #{response.code}!"
          end
        end

        def access_denied(response)
          if response.headers['WWW-Authenticate']
            fail NoSession
          else
            fail Unauthorized
          end
        end

        def full_url_for(path)
          api_url + api_base + path + '.json'
        end

        def api_base
          '/api/v1'
        end

        def basic_auth
          {username: username, password: password}
        end
      end
    end
  end
end
