# frozen_string_literal: true

module Researchable
  module Template
    # @api private
    class Endpoint < ActiveInteraction::Base
      object :basic_auth_session,
             default: -> { Researchable::Template.basic_auth_session },
             class: Researchable::Template::Sessions::BasicAuthSession

      def validate_response_for
        response = yield
        return deal_with_errors if response.parsed_response.is_a?(Hash) && response['errors'].present?
        return deal_with_unprocessable_entity if response.code == 422
        response_to_result response
      end

      # This function is used so it can be mocked later, and so
      # that it can be specified for each subclass
      def response_to_result(response)
        response
      end

      private

      def deal_with_unprocessable_entity
        errors.add :base, 'Validations failed!'
        nil
      end

      def deal_with_errors
        response['errors'].each do |attribute, attribute_errors|
          attribute_errors.each do |error|
            if respond_to?(attribute.to_sym) || attribute.to_sym == :base
              errors.add attribute.to_sym, error
            else
              errors.add :base, error
            end
          end
        end
        nil
      end
    end
  end
end
