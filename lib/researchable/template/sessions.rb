require 'httparty'
require 'researchable/template/sessions/basic_auth_session'

module Researchable
  module Template
    def self.basic_auth_session(*arguments, &block)
      Sessions::BasicAuthSession.new(*arguments, &block)
    end
  end
end
