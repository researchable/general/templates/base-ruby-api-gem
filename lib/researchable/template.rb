# frozen_string_literal: true
require 'active_interaction'
require 'researchable/template/version'
require 'researchable/template/sessions'
require 'researchable/template/endpoint'

module Researchable
  module Template
  end
end
