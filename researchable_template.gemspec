# frozen_string_literal: true

require File.expand_path('lib/researchable/template/version', __dir__)

Gem::Specification.new do |gem|
  gem.name          = 'researchable-template'
  gem.version       = Researchable::Template::VERSION
  gem.license       = 'Unlicensed'
  gem.summary       = 'Gem for calling the prediction API'
  gem.authors       = ['Frank Blaauw']
  gem.email         = 'template@researchable.nl'

  gem.files         = `git ls-files`.split($INPUT_RECORD_SEPARATOR)
  gem.executables   = gem.files.grep(%r{^bin/}).map { |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ['lib']

  gem.add_dependency 'active_interaction'
  gem.add_dependency 'httparty'

  gem.add_development_dependency 'appraisal'
  gem.add_development_dependency 'bundler'
  gem.add_development_dependency 'rake'
  gem.add_development_dependency 'rspec'
  gem.add_development_dependency 'yard'
  gem.add_development_dependency 'factory_bot'
end
